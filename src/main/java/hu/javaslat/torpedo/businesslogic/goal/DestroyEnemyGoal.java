package hu.javaslat.torpedo.businesslogic.goal;

import hu.javaslat.torpedo.Config;
import hu.javaslat.torpedo.businesslogic.command.ShootCommand;
import hu.javaslat.torpedo.interfaces.Command;
import hu.javaslat.torpedo.interfaces.Goal;
import hu.javaslat.torpedo.model.Entity;
import hu.javaslat.torpedo.model.Position;
import hu.javaslat.torpedo.model.Shoot;
import hu.javaslat.torpedo.model.geometry.Geometry;
import hu.javaslat.torpedo.model.wrapper.SimpleSubmarine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DestroyEnemyGoal implements Goal {

    private static final Logger LOGGER = LoggerFactory.getLogger(DestroyEnemyGoal.class);

    @Override
    public Command getAction(SimpleSubmarine submarine) {

        LOGGER.debug("submarine.id(): {}", submarine.getId());
        LOGGER.debug("submarine x: {}", submarine.getPosition().getX());
        LOGGER.debug("submarine y: {}", submarine.getPosition().getY());
        LOGGER.debug("getTorpedoExplosionRadius: {}", submarine.getGameContext().getGame().getMapConfiguration().getTorpedoExplosionRadius());
        LOGGER.debug("getTorpedoRange: {}", submarine.getGameContext().getGame().getMapConfiguration().getTorpedoRange());
        LOGGER.debug("getSubmarineSize: {}", submarine.getGameContext().getGame().getMapConfiguration().getSubmarineSize());
        LOGGER.debug("submarine.getTorpedoCooldown(): {}", submarine.getTorpedoCooldown());
        if (submarine.getTorpedoCooldown() == 0) {

            for (Entity entity : submarine.getGameContext().getEntities().getEnemySubmarineForSubmarine(submarine.getId())) {
                LOGGER.debug("Enemy id: {}", entity.getId());
                LOGGER.debug("Enemy x: {}", entity.getPosition().getX());
                LOGGER.debug("Enemy y: {}", entity.getPosition().getY());
                Shoot shoot = new Shoot();

                double angle = getAngle(submarine, entity);

                if (angle == 0) {
                    return null;
                }

                shoot.setAngle(angle);
                LOGGER.debug("AngleToEnemy: {}", shoot.getAngle());
                return new ShootCommand(submarine.getGameController(), submarine.getId(), shoot);
            }
        }
        return null;
    }

    private double getAngle(SimpleSubmarine submarine, Entity enemy) {

        Position enemyTempPosition = enemyOrSubmarineAfterStep(enemy.getPosition(), enemy.getAngle(), enemy.getVelocity());
        Position submarineTempPositon = enemyOrSubmarineAfterStep(submarine.getPosition(), submarine.getAngle(), submarine.getVelocity());

        //enemynek a következő poziciójába lövünk
        enemyTempPosition = enemyOrSubmarineAfterStep(enemyTempPosition, enemy.getAngle(), enemy.getVelocity());

        for (int i = 0; i < 1; i++) {
            double directionToOppenentX = enemyTempPosition.getX() - submarineTempPositon.getX();
            double directionToOppenentY = enemyTempPosition.getY() - submarineTempPositon.getY();
            LOGGER.debug("directionToOppenentX: {}", directionToOppenentX);
            LOGGER.debug("directionToOppenentY: {}", directionToOppenentY);
            Position directionToOppenent = new Position(directionToOppenentX, directionToOppenentY);

            double angleRadian = Geometry.computeAngle(directionToOppenent, new Position(1, 0));
            double angleDegree = Geometry.radianToDegree(angleRadian);
            LOGGER.debug("AngleToEnemy degree: {}", angleDegree);
            double sinAlfa = Geometry.crossProduct(new Position(1, 0), directionToOppenent);
            LOGGER.debug("Degree position: {}", sinAlfa);
            if (sinAlfa < 0) {
                angleDegree = 360 - angleDegree;
            }

            if (torpedoWillDestroyEnemy(submarineTempPositon, enemyTempPosition)) {
                LOGGER.debug("AngleToEnemy degree: {}", angleDegree);
                return angleDegree;
            }

            //léptetjük őket egy körrel, hátha akkor le tudjuk lőni a torpedóval
            enemyTempPosition = enemyOrSubmarineAfterStep(enemyTempPosition, enemy.getAngle(), enemy.getVelocity());
            submarineTempPositon = enemyOrSubmarineAfterStep(submarineTempPositon, submarine.getAngle(), submarine.getVelocity());
        }
        return 0;
    }

    private Position enemyOrSubmarineAfterStep(Position position, float angle, float velocity) {

        Position direction = Geometry.angleToVector(angle);
        Position distance = new Position(direction.getX() * velocity, direction.getY() * velocity);
        Position enemyOrSubmarineAfterStep = new Position(distance.getX() + position.getX(), distance.getY() + position.getY());
        LOGGER.debug("enemyOrSubmarineAfterStep directionX: {}, directionY: {}", direction.getX(), direction.getY());
        LOGGER.debug("enemyOrSubmarineAfterStep distanceX: {}, distanceY: {}", distance.getX(), distance.getY());
        LOGGER.debug("enemyOrSubmarineAfterStep enemyOrSubmarineAfterStepX: {}, enemyOrSubmarineAfterStepY: {}", enemyOrSubmarineAfterStep.getX(), enemyOrSubmarineAfterStep.getY());
        return enemyOrSubmarineAfterStep;
    }

    private boolean torpedoWillDestroyEnemy(Position submarineLaunchPosition, Position enemyArrivingPosition) {

        Position direction = new Position(enemyArrivingPosition.getX() - submarineLaunchPosition.getX(), enemyArrivingPosition.getY() - submarineLaunchPosition.getY());
        double distanceBetweenSubmarines = Geometry.length(direction);
        LOGGER.debug("torpedoWillDestroyEnemy distanceBetweenSubmarines: {}", distanceBetweenSubmarines);

        if (distanceBetweenSubmarines > Config.allowedDistance) {
            return true;
        }
        return false;
    }
}

package hu.javaslat.torpedo.businesslogic.goal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hu.javaslat.torpedo.Config;
import hu.javaslat.torpedo.businesslogic.command.MoveCommand;
import hu.javaslat.torpedo.interfaces.Command;
import hu.javaslat.torpedo.interfaces.Goal;
import hu.javaslat.torpedo.model.IslandPosition;
import hu.javaslat.torpedo.model.Move;
import hu.javaslat.torpedo.model.Position;
import hu.javaslat.torpedo.model.Road;
import hu.javaslat.torpedo.model.geometry.Geometry;
import hu.javaslat.torpedo.model.wrapper.SimpleSubmarine;

public class ExploreGoal implements Goal {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExploreGoal.class);
	private Road road;

	public ExploreGoal(Road road) {
		this.road = road;
	}

	@Override
	public Command getAction(SimpleSubmarine submarine) {

		Move move = new Move();

		setSpeed(submarine,move);
		setAngle(submarine,move);
		

		return new MoveCommand(submarine.getGameController(),submarine.getId(),move);
	}
	
	private void setSpeed(SimpleSubmarine submarine, Move move){

		float maxVelocity = submarine.getGameContext()
				.getGame()
				.getMapConfiguration()
				.getMaxSpeed();
		
		float maxVelocityPerRound = submarine.getGameContext()
				.getGame()
				.getMapConfiguration()
				.getMaxAccelerationPerRound();
		
		if (submarine.getVelocity() < maxVelocity) {

			if(maxVelocity - submarine.getVelocity() < maxVelocityPerRound){
				
				move.setSpeed(maxVelocity - submarine.getVelocity());
				
			} else {
				
				move.setSpeed(maxVelocityPerRound);
				
			}
			
		}
		
	}
	
	private void setAngle(SimpleSubmarine submarine, Move move){

		Position checkpoint = road.getNextCheckpoint();
		
		Position currentPos = submarine.getPosition();
		
		double distanceX = checkpoint.getX() - currentPos.getX();
		double distanceY = checkpoint.getY() - currentPos.getY();
		
		Position distance = new Position(distanceX,distanceY);
		
		if(Geometry.length(distance) <= 100 ){
			
			checkpoint = road.rotateCheckpoints();
			
		}

		double directionX = checkpoint.getX() - currentPos.getX();
		double directionY = checkpoint.getY() - currentPos.getY();
		
		Position direction = new Position(directionX, directionY);
		Position submarineOrientation = Geometry.angleToVector(submarine.getAngle());
		
		IslandPosition islandPos = submarine.getGameContext().getGame().getMapConfiguration().getIslandPositions().get(0);
		Position IslandCenter = new Position(islandPos.getX(),islandPos.getY());
		double IslandRadius = submarine.getGameContext().getGame().getMapConfiguration().getIslandSize();

		double turn = 0;
		
		double maxTurnInRound = submarine.getGameContext()
				.getGame()
				.getMapConfiguration()
				.getMaxSteeringPerRound();	
		

		LOGGER.info("submarine id: {}", submarine.getId());
		LOGGER.info("current pos: {}", currentPos);
		LOGGER.info("checkpoint pos: {}", checkpoint);
		
		double width = submarine.getGameContext().getGame().getMapConfiguration().getWidth();
		double height = submarine.getGameContext().getGame().getMapConfiguration().getHeight();
		
		Position transformedCurrentPos = new Position(currentPos.getX() - width/2,currentPos.getY() - height/ 2);
		Position transformedCheckpoint = new Position(checkpoint.getX() - width/2,checkpoint.getY() - height/ 2);

		LOGGER.info("transformed current pos: {}", transformedCurrentPos);
		LOGGER.info("transformed checkpoint pos: {}", transformedCheckpoint);
		
		boolean isIntersect = Geometry.isLineIntersectCircle(transformedCurrentPos, transformedCheckpoint, new Position(0,0), IslandRadius);
		double radianToIsland = Geometry.computeAngle(Geometry.angleToVector(submarine.getAngle()), Geometry.getDifferenceVector(currentPos, IslandCenter));
		LOGGER.info("is intersect Island: {}", isIntersect);
		
		if(isIntersect && Geometry.radianToDegree(radianToIsland) < 90
				&& Geometry.length(Geometry.getDifferenceVector(currentPos, IslandCenter)) < IslandRadius*2){
			
			move.setTurn(maxTurnInRound);
			
		} else {
			
			double angleRadian = Geometry.computeAngle(direction, submarineOrientation);
			double angleDegree = Geometry.radianToDegree(angleRadian);
					
			if(angleDegree < maxTurnInRound)
				turn = angleDegree;
			else
				turn = maxTurnInRound;
			
			Position newSubmarinOrientationP = Geometry.angleToVector(submarine.getAngle() + turn);
			Position newSubmarinOrientationN = Geometry.angleToVector(submarine.getAngle() - turn);
			
			if(Geometry.computeAngle(direction, newSubmarinOrientationP) < Geometry.computeAngle(direction, newSubmarinOrientationN)){
				move.setTurn(turn);
			} else {
				move.setTurn(-turn);
			}
			
		}
		
	}
	

}

package hu.javaslat.torpedo.businesslogic.goal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hu.javaslat.torpedo.Config;
import hu.javaslat.torpedo.businesslogic.command.MoveCommand;
import hu.javaslat.torpedo.interfaces.Command;
import hu.javaslat.torpedo.interfaces.Goal;
import hu.javaslat.torpedo.model.Entity;
import hu.javaslat.torpedo.model.Move;
import hu.javaslat.torpedo.model.Position;
import hu.javaslat.torpedo.model.geometry.Geometry;
import hu.javaslat.torpedo.model.wrapper.Entities;
import hu.javaslat.torpedo.model.wrapper.SimpleSubmarine;

public class FollowEnemyGoal implements Goal {

    private static final Logger LOGGER = LoggerFactory.getLogger(FollowEnemyGoal.class);
	private Long enemySubmarineId = -1L;

	@Override
	public Command getAction(SimpleSubmarine submarine) {
		Move move = new Move();

		setMove(submarine,move);
		setAngle(submarine,move);
		
		return new MoveCommand(submarine.getGameController(),submarine.getId(),move);
	
	}
	
	private void setMove(SimpleSubmarine submarine, Move move) {
		Entity enemy = submarine.getGameContext().getEntities().getEnemySubmarineById(enemySubmarineId);
//		double sonarRadius = submarine.getGameContext().getGame().getMapConfiguration().getSonarRange();
		
		Position submarineOrientation = Geometry.angleToVector(submarine.getAngle());
		Position enemyOrientation = Geometry.angleToVector(enemy.getAngle());
		float maxVelocity = submarine.getGameContext()
				.getGame()
				.getMapConfiguration()
				.getMaxSpeed();
		
		float maxVelocityPerRound = submarine.getGameContext()
				.getGame()
				.getMapConfiguration()
				.getMaxAccelerationPerRound();
		
		
		if(Geometry.radianToDegree(Geometry.computeAngle(submarineOrientation, enemyOrientation)) > 90){
			
			if (submarine.getVelocity() > maxVelocity/2) {
				
					move.setSpeed(-maxVelocityPerRound);
					
			}
			
		} else {
			
			double submarineEnemyDistance = Geometry.length(Geometry.getDifferenceVector(submarine.getPosition(), enemy.getPosition()));
			
			if(submarineEnemyDistance < Config.allowedDistance){
				
				if (submarine.getVelocity() > maxVelocity/2) {
					
					double decreaseSpeed = submarineEnemyDistance / Config.allowedDistance;
					
					move.setSpeed(-decreaseSpeed * maxVelocityPerRound);

					LOGGER.info("Following enemy goal, decrease speed: {}", move.getSpeed());
				}
				
			} else {
				
				if(maxVelocity - submarine.getVelocity() < maxVelocityPerRound){
					
					move.setSpeed(maxVelocity - submarine.getVelocity());
					
				} else {
					
					move.setSpeed(maxVelocityPerRound);
					
				}
				
			}
			
			
			
		}
	}

	public void setEnemySubmarineId(Long enemySubmarineId){
		this.enemySubmarineId = enemySubmarineId;
	}

	private void setAngle(SimpleSubmarine submarine, Move move) {
		Entities entities = submarine.getGameController().getGameContext().getEntities();
		
		Entity enemy = entities.getEnemySubmarineById(enemySubmarineId);
		
		double directionX = enemy.getPosition().getX() - submarine.getPosition().getX();
		double directionY = enemy.getPosition().getY() - submarine.getPosition().getY();
				
		Position direction = new Position(directionX, directionY);
		Position submarineOrientation = Geometry.angleToVector(submarine.getAngle());

		double angleRadian = Geometry.computeAngle(direction, submarineOrientation);
		double angleDegree = Geometry.radianToDegree(angleRadian);
				
		double turn = 0;
		
		double maxTurnInRound = submarine.getGameContext()
				.getGame()
				.getMapConfiguration()
				.getMaxSteeringPerRound();

		if(angleDegree < maxTurnInRound)
			turn = angleDegree;
		else
			turn = maxTurnInRound;
		
		Position newSubmarinOrientationP = Geometry.angleToVector(submarine.getAngle() + turn);
		Position newSubmarinOrientationN = Geometry.angleToVector(submarine.getAngle() - turn);
		
		if(Geometry.computeAngle(direction, newSubmarinOrientationP) < Geometry.computeAngle(direction, newSubmarinOrientationN)){
			move.setTurn(turn);
		} else {
			move.setTurn(-turn);
		}

	}

}

package hu.javaslat.torpedo.businesslogic.command;

import hu.javaslat.torpedo.GameController;
import hu.javaslat.torpedo.client.responses.UseActiveSonarResponse;
import hu.javaslat.torpedo.interfaces.Command;
import hu.javaslat.torpedo.interfaces.TorpedoService;

public class ExtendSonarCommand implements Command {
    
    private GameController controller;
    private long submarineId;

    public ExtendSonarCommand(GameController controller, long submarineId) {
    	this.controller = controller;
        this.submarineId = submarineId;
    }
    
    @Override
    public boolean execute() {
    	TorpedoService torpedoService = controller.getTorpedoService();
    	long gameId = controller.getGameContext().getGame().getId();
    	
        UseActiveSonarResponse useActiveSonarResponse = torpedoService.useActiveSonar(gameId, submarineId);
        return useActiveSonarResponse.getCode() == 0;
    }
    
}

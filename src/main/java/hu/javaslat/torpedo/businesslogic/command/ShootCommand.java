package hu.javaslat.torpedo.businesslogic.command;

import hu.javaslat.torpedo.GameController;
import hu.javaslat.torpedo.client.responses.ShootResponse;
import hu.javaslat.torpedo.interfaces.Command;
import hu.javaslat.torpedo.interfaces.TorpedoService;
import hu.javaslat.torpedo.model.Shoot;

public class ShootCommand implements Command {

    private GameController controller;
    private long submarineId;
    private Shoot shoot;

    public ShootCommand(GameController controller, long submarineId, Shoot shoot) {
        this.controller = controller;
        this.submarineId = submarineId;
        this.shoot = shoot;
    }
    
    @Override
    public boolean execute() {
    	
		TorpedoService torpedoService = controller.getTorpedoService();
		long gameId = controller.getGameContext().getGame().getId();
		
        ShootResponse shootResponse = torpedoService.shoot(gameId, submarineId, shoot);
        return shootResponse.getCode() == 0;
    }
    
}

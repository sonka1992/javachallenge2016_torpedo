package hu.javaslat.torpedo.businesslogic.command;

import hu.javaslat.torpedo.GameController;
import hu.javaslat.torpedo.client.responses.MoveResponse;
import hu.javaslat.torpedo.interfaces.Command;
import hu.javaslat.torpedo.interfaces.TorpedoService;
import hu.javaslat.torpedo.model.Move;

public class MoveCommand implements Command {

    private GameController controller;
    private long submarineId;
    private Move move;

    public MoveCommand(GameController controller, long submarineId, Move move) {
        this.controller = controller;
        this.submarineId = submarineId;
        this.move = move;
    }
    
    @Override
    public boolean execute() {
    	
		TorpedoService torpedoService = controller.getTorpedoService();
		long gameId = controller.getGameContext().getGame().getId();
    	
		MoveResponse moveResponse = torpedoService.move(gameId, submarineId, move);
		return moveResponse.getCode() == 0;
    }
    
}

package hu.javaslat.torpedo.gui;

import java.util.List;

import hu.javaslat.torpedo.Config;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

@SuppressWarnings("restriction")
public class GamesList extends VBox {
	
	private ListView<String> list;
	private ObservableList<String> items;
	
	public GamesList(GameControllerWithView controller){
		super();
		
		Button listGamesButton = new Button();
		listGamesButton.setText("List\u00E1z");
		listGamesButton.setMinWidth(Config.GUI.BUTTON_WIDTH);
		listGamesButton.setMaxWidth(Config.GUI.BUTTON_WIDTH);
		
		listGamesButton.setOnAction( e -> {
        	List<Long> ids = controller.listGames();
        	
        	items.clear();
        	
        	for(Long id : ids){
        		items.add(Long.toString(id));
        	}
        });

		Button createGameButton = new Button();
		createGameButton.setText("L\u00E9trehoz");
		createGameButton.setMinWidth(Config.GUI.BUTTON_WIDTH);
		createGameButton.setMaxWidth(Config.GUI.BUTTON_WIDTH);
		
		createGameButton.setOnAction( e -> {
        	Long id = controller.createGame();
        	
        	items.add(Long.toString(id));
        });
        
        Button joinGameButton = new Button();
        joinGameButton.setText("Csatlakoz\u00E1s");
        joinGameButton.setMinWidth(Config.GUI.BUTTON_WIDTH);
        joinGameButton.setMaxWidth(Config.GUI.BUTTON_WIDTH);
        
        joinGameButton.setOnAction( e -> {
        	String stringId = list.getSelectionModel().getSelectedItem();
        	        	
        	controller.joinGame(Long.parseLong(stringId));
        });
        
        list = new ListView<String>();
		items = FXCollections.observableArrayList();

		list.setItems(items);
		list.setMaxHeight(100);
		
		HBox firstRowButtons = new HBox();
		firstRowButtons.setSpacing(20);
		firstRowButtons.getChildren().addAll(createGameButton,listGamesButton);
		firstRowButtons.setAlignment(Pos.BASELINE_CENTER);
		
		HBox secondRowButtons = new HBox();
		secondRowButtons.getChildren().addAll(joinGameButton);
		secondRowButtons.setAlignment(Pos.BASELINE_CENTER);

		
		this.getChildren().addAll(list,firstRowButtons,secondRowButtons);
		this.setSpacing(Config.GUI.GAME_LIST_SPACING);
	}

}

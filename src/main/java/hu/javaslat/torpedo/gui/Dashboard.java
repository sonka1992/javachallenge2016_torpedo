package hu.javaslat.torpedo.gui;

import java.util.HashMap;
import java.util.Map;

import hu.javaslat.torpedo.Config;
import hu.javaslat.torpedo.model.Game;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

@SuppressWarnings("restriction")
public class Dashboard extends VBox{
	
	private GameControllerWithView controller;
	
	private final Label idLabel = new Label("Game id: ");	
	private final Label roundLabel = new Label("Round: ");
	private final Label scoresLabel = new Label("Scores: ");
	
	private Label idValue;
	private Label roundValue;
	private Map<String, Label> teamScores = new HashMap<>();
		
	public Dashboard(GameControllerWithView controller){
		super();
		
		this.controller = controller;

		idValue = new Label("");
		roundValue = new Label("0");
		
		HBox idRow = new HBox();
		idRow.getChildren().addAll(idLabel, idValue);
		idRow.setAlignment(Pos.BASELINE_CENTER);
		
		HBox roundRow = new HBox();
		roundRow.getChildren().addAll(roundLabel, roundValue);
		roundRow.setAlignment(Pos.BASELINE_CENTER);

		this.getChildren().add(idRow);
		this.getChildren().add(roundRow);
		this.getChildren().add(scoresLabel);
		
		this.setSpacing(Config.GUI.DASHBOARD_SPACING);
	}
	
	public void reset(){
		this.getChildren().clear();
		Game game = controller.getGameContext().getGame();

		HBox idRow = new HBox();
		idValue.setText(Long.toString(game.getId()));
		idRow.getChildren().addAll(idLabel, idValue);
		idRow.setAlignment(Pos.BASELINE_CENTER);

		HBox roundRow = new HBox();
		roundRow.getChildren().addAll(roundLabel, roundValue);
		roundRow.setAlignment(Pos.BASELINE_CENTER);

		this.getChildren().add(idRow);
		this.getChildren().add(roundRow);
		this.getChildren().add(scoresLabel);
	}
	
	public void refresh(){
		
		Game game = controller.getGameContext().getGame();
				
		idValue.setText(Long.toString(game.getId()));
		roundValue.setText(Long.toString(game.getRound()));
		
		Label tempLabel;

		for(Map.Entry<String, Long> entry : game.getScores().getScores().entrySet()){

			if(teamScores.containsKey(entry.getKey())){
				tempLabel = teamScores.get(entry.getKey());
				tempLabel.setText(entry.getKey() + ": " + Long.toString(entry.getValue()));
			} else {
				tempLabel = new Label(entry.getKey() + ": " + Long.toString(entry.getValue()));
				teamScores.put(entry.getKey(), tempLabel);
				
				HBox temp = new HBox();
				temp.getChildren().add(tempLabel);
				temp.setAlignment(Pos.BASELINE_CENTER);
				
				this.getChildren().add(temp);
			}
			
		}
				
	}
	
}

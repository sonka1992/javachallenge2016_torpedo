 package hu.javaslat.torpedo.gui;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hu.javaslat.torpedo.Config;
import hu.javaslat.torpedo.GameController;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

@SuppressWarnings("restriction")
public class GameControllerWithView extends GameController{

	private static final Logger LOGGER = LoggerFactory.getLogger(GameControllerWithView.class);
	private Dashboard dashboard;
	private TorpedoGUI torpedoGUI;
	private ControlPanel controlPanel;
	
	public GameControllerWithView(Stage primaryStage){

		super();
		
		BorderPane root = new BorderPane();
		
		dashboard = new Dashboard(this);

		torpedoGUI = new TorpedoGUI(this);
		
		controlPanel = new ControlPanel(this); 

		root.setCenter(torpedoGUI);
		
		VBox rightPanel = new VBox();
		rightPanel.setSpacing(10);
		
		rightPanel.getChildren().addAll(new GamesList(this), dashboard);

		root.setRight(rightPanel);
		
		VBox leftPanel = new VBox();
		leftPanel.setSpacing(10);
		
		
		leftPanel.getChildren().add(controlPanel);
		leftPanel.setMinWidth(Config.GUI.SIDE_PANEL_WIDTH);
		
		root.setLeft(leftPanel);
		
		int sumWidth = Config.GUI.GAME_MAP_WIDTH + Config.GUI.SIDE_PANEL_WIDTH;
		sumWidth += Config.GUI.SIDE_PANEL_WIDTH;
		
		Scene scene2 = new Scene(root, sumWidth, Config.GUI.GAME_MAP_HEIGHT, Color.BLACK);
		
		primaryStage.setScene(scene2);
	}
	
	public Long createGame(){
		
		return super.createGame();
	}
	
	public void joinGame(long gameId){
		super.joinGame(gameId);
		
		dashboard.reset();
		dashboard.refresh();
		
		controlPanel.reset();
		controlPanel.refresh();
		
		torpedoGUI.refresh();                
				
		super.play(gameId);
	}
	
	public void draw(){

		Platform.runLater(() -> {
			
			dashboard.refresh();
			controlPanel.refresh();						
			torpedoGUI.refresh();
			
		});
		
	}
	
	public List<Long> listGames(){
		return super.listGames();
	}
	
}

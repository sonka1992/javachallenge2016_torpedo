package hu.javaslat.torpedo;

import hu.javaslat.torpedo.businesslogic.goal.DestroyEnemyGoal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hu.javaslat.torpedo.businesslogic.goal.ExploreGoal;
import hu.javaslat.torpedo.businesslogic.goal.Roads;
import hu.javaslat.torpedo.client.implementation.BankTechTorpedoService;
import hu.javaslat.torpedo.client.responses.CreateGameResponse;
import hu.javaslat.torpedo.client.responses.ListGamesResponse;
import hu.javaslat.torpedo.interfaces.TorpedoService;
import hu.javaslat.torpedo.model.ConnectionStatus;
import hu.javaslat.torpedo.model.Entity;
import hu.javaslat.torpedo.model.Move;
import hu.javaslat.torpedo.model.Shoot;
import hu.javaslat.torpedo.model.Submarine;
import hu.javaslat.torpedo.model.wrapper.GameContext;
import hu.javaslat.torpedo.model.wrapper.SimpleSubmarine;
import java.util.HashSet;
import java.util.Set;
import javafx.concurrent.Task;

@SuppressWarnings("restriction")
public class GameController {

	private static final Logger LOGGER = LoggerFactory.getLogger(GameController.class);
	protected TorpedoService torpedoService;
	protected GameContext gameContext;
	public boolean stop = false;

	public GameController() {

		gameContext = new GameContext();
		torpedoService = new BankTechTorpedoService();

	}

	protected Long createGame() {
		CreateGameResponse res = torpedoService.createGame();
		return res.getId();
	}

	protected List<Long> listGames() {
		ListGamesResponse res = torpedoService.listGames();
		return res.getGames();
	}

	private boolean isConnected(long gameId) {
		ConnectionStatus connectionStatus = torpedoService.getGameInfo(gameId).getGame().getConnectionStatus();
		return connectionStatus.getConnected().get(Config.TEAM_NAME).booleanValue();
	}

	protected void joinGame(long gameId) {

		if (!isConnected(gameId)) {

			torpedoService.joinGame(gameId);

		}

		populateGameContext(gameId);

	}

	protected void play(long gameId) {

		Task<Void> task = new Task<Void>() {

			@Override
			protected Void call() throws Exception {

				while (gameContext.getGame().getRound() < gameContext.getGame().getMapConfiguration().getRounds()
						&& !stop) {
                                        try{
                                            LOGGER.info("new round: " + gameContext.getGame().getRound());
                                            populateGameContext(gameId);

                                            for (SimpleSubmarine simpleSubmarine : gameContext.getSubmarines().values()) {
                                                    simpleSubmarine.play();
                                            }
                                        }catch(Exception e){
                                            LOGGER.error("Exception", e);
                                        }
                                        
                                        draw();

					Thread.sleep(2000);

				}
				return null;

			}
		};

		Thread th = new Thread(task);

		th.start();
	}

	protected void populateGameContext(long gameId) {

		gameContext.setGame(torpedoService.getGameInfo(gameId).getGame());
		
		double torpedoVelocity = gameContext.getGame().getMapConfiguration().getTorpedoSpeed();
		Config.allowedDistance = 2 * torpedoVelocity - 5;

		List<Submarine> submarines = torpedoService.getSubmarines(gameId).getSubmarines();
                Set<Long> actualSubmarineIds = new HashSet<Long>();
                actualSubmarineIds.addAll(gameContext.getSubmarines().keySet());
		boolean exploreClockWise = true;
		
		
		for (Submarine submarine : submarines) {
			if (gameContext.getSubmarines().containsKey(submarine.getId())) {

				SimpleSubmarine temp = gameContext.getSubmarines().get(submarine.getId());
				temp.setSubmarine(submarine);
				gameContext.getSubmarines().put(submarine.getId(), temp);

			} else {

				ExploreGoal explore;
				
				if(exploreClockWise){
					explore = new ExploreGoal(Roads.clockwise);
				} else {
					explore = new ExploreGoal(Roads.antiClockwise);
				}

				exploreClockWise = !exploreClockWise;
				
				gameContext.getSubmarines().put(submarine.getId(), new SimpleSubmarine(this, submarine, explore, new DestroyEnemyGoal()));

			}

		}
                
                
		
		for(Long actualSabmarine : actualSubmarineIds) {
			LOGGER.debug("actualSubmarineIds actualSabmarine: {}", actualSabmarine);
			boolean isAlive = false;
			
			for (Submarine aliveSubmarine : submarines){
                            if(aliveSubmarine.getId().equals(actualSabmarine)){
                                    isAlive = true;
                            }
			}
			
			if(!isAlive){
                            LOGGER.debug("Sajnos meghalt a submarinunk! Id: " + actualSabmarine);
                            gameContext.getSubmarines().remove(actualSabmarine);
                            gameContext.getEntities().getViews().remove(actualSabmarine);
                        }
			
		}

		List<Entity> entities;

		for (Submarine submarine : gameContext.getSubmarines().values()) {
			entities = new ArrayList<>();
			entities.addAll(torpedoService.usePassiveSonar(gameId, submarine.getId()).getEntities());
			gameContext.getEntities().put(submarine.getId(), entities);
		}

	}

	public void move(long gameId, long submarineId, Move move) {
		torpedoService.move(gameId, submarineId, move);
	}

	public void fire(long gameId, long submarineId, Shoot shoot) {
		torpedoService.shoot(gameId, submarineId, shoot);
	}

	public GameContext getGameContext() {
		return gameContext;
	}
	
	public TorpedoService getTorpedoService(){
		return torpedoService;
	}

	public void draw() {

		throw new UnsupportedOperationException("Logic not support draw");

	}
}

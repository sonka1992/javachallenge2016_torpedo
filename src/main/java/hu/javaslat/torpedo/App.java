package hu.javaslat.torpedo;

import hu.javaslat.torpedo.gui.GameControllerWithView;
import javafx.application.Application;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("restriction")
public class App extends Application {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        
        if(args != null && args.length == 1) {
            try {
                String arguments = args[0].replaceAll("\"", "");
                String[] argumentsInTwoPieces = arguments.split(":");
                Config.init(argumentsInTwoPieces[0], argumentsInTwoPieces[1]);
            } catch (Exception e) {
                LOGGER.error("Error init the application!", e);
                System.exit(1);
            }
            
            launch(args);
        } else {
            LOGGER.error("Nem megfelelo parameterekkel tortent az inditas! Megfelelo inditas peldaul: java -jar javaslat.jar \"localhost:8080\"");
            System.exit(1);
        }
        
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        GameControllerWithView view = new GameControllerWithView(primaryStage);

        primaryStage.setOnCloseRequest(e -> {
            view.stop = true;
        });
        primaryStage.setResizable(false);
        primaryStage.setTitle("BankTech JavaChallenge 2016 - Torpedo - Javaslat");
        primaryStage.show();

    }
}

package hu.javaslat.torpedo.model.geometry;

import hu.javaslat.torpedo.model.Position;

public class Geometry {
	
	public static double PI = 3.14;
	
	public static double computeAngle (Position v0, Position v1)
	{
		
	  double dotProduct = Geometry.computeDotProduct (v0, v1);

	  double length1 = Geometry.length (v0);
	  double length2 = Geometry.length (v1);

	  double denominator = length1 * length2;

	  double product = denominator != 0.0 ? dotProduct / denominator : 0.0;

	  double angle = Math.acos (product);
	  
	  return angle;
	  
	}
	
	/**
	 * Elcsalt cross product valójában az eredmény vektorának a z-komponensét adja vissza
	 * @param v0
	 * @param v1
	 * @return
	 */
	public static double crossProduct(Position v0, Position v1){
		double length1 = Geometry.length (v0);
		double length2 = Geometry.length (v1);
		
		double denominator = length1 * length2;
		  
		double crossProduct = (v0.getX() * v1.getY()) - (v0.getY() * v1.getX());
		
		double sinAlfa = denominator != 0.0 ? crossProduct / denominator : 0.0;
		  
		return sinAlfa;
	}
	
	public static double length(Position v){
		double a = v.getX() * v.getX();
		double b = v.getY() * v.getY();
		
		double length = Math.sqrt(a + b);
		
		return length;
		
	}
	
	public static double computeDotProduct(Position v0, Position v1){
		
		return v0.getX() * v1.getX() + v0.getY() * v1.getY();
		
	}
	
	public static double radianToDegree(double angle){
		
		return angle * 180 / PI;
		
	}
	
	public static double degreeToRadian(double angle){
		
		return angle * PI / 180;
		
	}
	
	public static Position angleToVector(double angle){
		
		double radian = degreeToRadian(angle);
		
		double x = Math.cos(radian);
		double y = Math.sin(radian);
		
		return new Position(x,y);
	}
	
	public static boolean isLineIntersectCircle(Position pointOnLine1, Position pointOnLine2, Position circleCenter, double circleRadius){
		boolean intersect = false;
		
		Position d = new Position(pointOnLine2.getX()-pointOnLine1.getX(),pointOnLine2.getY()-pointOnLine1.getY());
		double dLength = length(d);
		
		double D = pointOnLine1.getX() * pointOnLine2.getY() - pointOnLine2.getX() * pointOnLine1.getY();
		
		double discriminant = circleRadius * circleRadius * dLength * dLength - D * D; 
		
		if(discriminant >= 0)
			intersect = true;
		
		return intersect;
	}
	
	public static Position getDifferenceVector(Position p0, Position p1){
		
		double x = p1.getX() - p0.getX();
		double y = p1.getY() - p0.getY();		
		
		return new Position(x,y);
	}
	
}

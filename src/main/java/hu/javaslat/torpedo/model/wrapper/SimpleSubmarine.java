package hu.javaslat.torpedo.model.wrapper;

import java.util.HashMap;
import java.util.Map;

import hu.javaslat.torpedo.GameController;
import hu.javaslat.torpedo.businesslogic.command.ExtendSonarCommand;
import hu.javaslat.torpedo.businesslogic.goal.DestroyEnemyGoal;
import hu.javaslat.torpedo.businesslogic.goal.ExploreGoal;
import hu.javaslat.torpedo.businesslogic.goal.FollowEnemyGoal;
import hu.javaslat.torpedo.interfaces.Command;
import hu.javaslat.torpedo.model.Action;
import hu.javaslat.torpedo.model.Position;
import hu.javaslat.torpedo.model.Submarine;
import hu.javaslat.torpedo.model.geometry.Geometry;

public class SimpleSubmarine extends Submarine {

    private Map<Action, Integer> remainedInvokation = new HashMap<Action, Integer>();
    private GameController controller;
    private ExploreGoal explore;
    private DestroyEnemyGoal destroyEnemy;
    private FollowEnemyGoal followEnemyGoal = new FollowEnemyGoal();
    private Long followedSubmarineId = -1L;
    
    public SimpleSubmarine(GameController controller, Submarine submarine, ExploreGoal explore, DestroyEnemyGoal destroyEnemy){
    	
    	resetRemainedInvokations();
    	this.explore = explore;
        this.destroyEnemy = destroyEnemy;
    	
    	this.controller = controller;
		this.type = submarine.getType();
		this.id = submarine.getId();
		this.position = submarine.getPosition();
		this.owner = submarine.getOwner();
		this.velocity = submarine.getVelocity();
		this.angle = submarine.getAngle();
		this.hp = submarine.getHp();
		this.sonarCooldown = submarine.getSonarCooldown();
		this.torpedoCooldown = submarine.getTorpedoCooldown();
		this.sonarExtended = submarine.getSonarExtended();
		
    }
    
    public void play() {
    	
        resetRemainedInvokations();
        Command command = explore.getAction(this);
        Command followEnemy = null;
        
        Entities sawEntities = controller.getGameContext().getEntities();
        
        if(remainedInvokation.get(Action.MOVE) > 0) {
        	
        	
        	if(!sawEntities.getEnemySubmarineForSubmarine(getId()).isEmpty()){
        		
        		if (sawEntities.getEnemySubmarineById(followedSubmarineId) == null){
        			followedSubmarineId = -1L;
        		}
        		
        		if (followedSubmarineId.equals(-1L)){
        			followedSubmarineId = sawEntities.getEnemySubmarineForSubmarine(getId()).get(0).getId();
        		}

        		if (getSonarCooldown() == 0){
        			Position submarineOrientation = Geometry.angleToVector(getAngle());
        			Position enemyOrientation = Geometry.angleToVector(sawEntities.getEnemySubmarineById(followedSubmarineId).getAngle());
        			
        			double sonarRadius = controller.getGameContext().getGame().getMapConfiguration().getSonarRange();
        			
        			if(Geometry.crossProduct(submarineOrientation, enemyOrientation) > 0
        					&& Geometry.radianToDegree(Geometry.computeAngle(submarineOrientation, enemyOrientation)) < 110
        					&& Geometry.length(new Position(getPosition().getX()-enemyOrientation.getX(),getPosition().getY()-enemyOrientation.getY())) > (sonarRadius * 0.6))
        				new ExtendSonarCommand(controller, getId()).execute();
        		}

        		followEnemyGoal.setEnemySubmarineId(followedSubmarineId);
        		followEnemy = followEnemyGoal.getAction(this);

        		if(followEnemy.execute()) {
                    remainedInvokation.put(Action.MOVE, remainedInvokation.get(Action.MOVE) - 1);
                }
        		
        	} else if(command.execute()) {
        		followedSubmarineId = -1L;
                remainedInvokation.put(Action.MOVE, remainedInvokation.get(Action.MOVE) - 1);
            }
        }
        
        if(remainedInvokation.get(Action.SHOOT) > 0) {
            Command shootCommand = destroyEnemy.getAction(this);
            if(shootCommand != null){
                if(shootCommand.execute()) {
                    remainedInvokation.put(Action.SHOOT, remainedInvokation.get(Action.SHOOT) - 1);
                }
            }
        }
        
    }
    
    public void setSubmarine(Submarine submarine){

		this.type = submarine.getType();
		this.id = submarine.getId();
		this.position = submarine.getPosition();
		this.owner = submarine.getOwner();
		this.velocity = submarine.getVelocity();
		this.angle = submarine.getAngle();
		this.hp = submarine.getHp();
		this.sonarCooldown = submarine.getSonarCooldown();
		this.torpedoCooldown = submarine.getTorpedoCooldown();
		this.sonarExtended = submarine.getSonarExtended();
		
    }
    
    public GameContext getGameContext(){
    	return controller.getGameContext();
    }
    
    public GameController getGameController(){
    	return controller;
    }

    private void resetRemainedInvokations() {

        remainedInvokation.put(Action.CREATE_GAME, -1);
        remainedInvokation.put(Action.LIST_GAMES, -1);
        remainedInvokation.put(Action.JOIN_GAME, -1);
        remainedInvokation.put(Action.GET_GAME_INFO, -1);
        remainedInvokation.put(Action.GET_SUBMARINES, -1);
        remainedInvokation.put(Action.MOVE, 1);
        remainedInvokation.put(Action.SHOOT, 1);
        remainedInvokation.put(Action.GET_SONAR, 1);
        remainedInvokation.put(Action.EXTEND_SONAR, 1);

    }
}

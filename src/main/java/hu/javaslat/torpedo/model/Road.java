package hu.javaslat.torpedo.model;

import java.util.ArrayDeque;
import java.util.Queue;

public class Road {
	
	private Queue<Position> checkpoints = new ArrayDeque<>();
	
	public void addPosition(Position pos){
		checkpoints.offer(pos);
	}
	
	public Position rotateCheckpoints(){
		Position next = checkpoints.poll();
		checkpoints.offer(next);
		
		return checkpoints.peek();
	}
	
	public Position getNextCheckpoint(){
		return checkpoints.peek();
	}

}

package hu.javaslat.torpedo.model;

import java.util.Map;

public class Scores {
	
	private Map<String,Long> scores;

	public Map<String,Long> getScores() {
		return scores;
	}

	public void setScores(Map<String,Long> scores) {
		this.scores = scores;
	}

	@Override
	public String toString() {
		return "Scores [scores=" + scores + "]";
	}
	
	

}

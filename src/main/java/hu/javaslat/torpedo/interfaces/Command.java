package hu.javaslat.torpedo.interfaces;

public interface Command {
    
    public boolean execute();
}

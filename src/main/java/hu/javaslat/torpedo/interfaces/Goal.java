package hu.javaslat.torpedo.interfaces;

import hu.javaslat.torpedo.model.wrapper.SimpleSubmarine;

public interface Goal {

	public Command getAction(SimpleSubmarine submarine);
	
}

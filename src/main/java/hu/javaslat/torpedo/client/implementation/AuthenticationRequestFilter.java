package hu.javaslat.torpedo.client.implementation;

import java.io.IOException;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;

import hu.javaslat.torpedo.Config;

public class AuthenticationRequestFilter implements ClientRequestFilter {

	@Override
	public void filter(ClientRequestContext requestContext) throws IOException {
        requestContext.getHeaders().add(Config.TEAM_TOKEN_NAME, Config.TEAM_TOKEN_VALUE);
	}

	
	
}

package hu.javaslat.torpedo.client.responses;

import java.util.List;

import hu.javaslat.torpedo.model.Entity;

public class UsePassiveSonarResponse {

	private List<Entity> entities;
	private String message;
	private int code;
	
	public List<Entity> getEntities() {
		return entities;
	}
	
	public void setEntities(List<Entity> entities) {
		this.entities = entities;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public int getCode() {
		return code;
	}
	
	public void setCode(int code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "UsePassiveSonarResponse [entities=" + entities + ", message=" + message + ", code=" + code + "]";
	}
	
}

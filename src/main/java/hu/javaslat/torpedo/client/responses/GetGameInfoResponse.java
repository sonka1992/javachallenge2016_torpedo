package hu.javaslat.torpedo.client.responses;

import hu.javaslat.torpedo.model.Game;

public class GetGameInfoResponse {
	
	private Game game;
	private String message;
	private int code;
	
	public Game getGame() {
		return game;
	}
	
	public void setGame(Game game) {
		this.game = game;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public int getCode() {
		return code;
	}
	
	public void setCode(int code) {
		this.code = code;
	}
	
	@Override
	public String toString() {
		return "GameInfo [game=" + game + ", message=" + message + ", code=" + code + "]";
	}

	
	
	
}
